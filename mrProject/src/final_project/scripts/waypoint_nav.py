#!/usr/bin/env python

'''
Copyright (c) 2015, Mark Silliman
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''

# TurtleBot must have minimal.launch & amcl_demo.launch
# running prior to starting this script
# For simulation: launch gazebo world & amcl_demo prior to run this script

import rospy
import tf
from move_base_example import GoToPose
from actionlib_msgs.msg import *
from geometry_msgs.msg import Pose, Point, Quaternion
from apriltags2_ros.msg import *


def move_to_goal(pose_x, pose_y):
    try:
        navigator = GoToPose()

        # Set waypoint location information
        position = {'x': pose_x, 'y' : pose_y}
        quaternion = {'r1' : 0.000, 'r2' : 0.000, 'r3' : 0.000, 'r4' : 1.000}

        rospy.loginfo("Go to (%s, %s) pose", position['x'], position['y'])
        success = navigator.goto(position, quaternion)

        if success:
            rospy.loginfo("Hooray, reached the desired pose")
            result = True
        else:
            rospy.loginfo("The base failed to reach the desired pose")
            result = False
        return result

        # Sleep to give the last log messages time to be sent
        rospy.sleep(1)

    except rospy.ROSInterruptException:
        rospy.loginfo("Ctrl-C caught. Quitting")


if __name__ == '__main__':
    # Initialize this node
    rospy.init_node('waypoint_nav')
    # Initialize tag database
    visited_tags = {}
    tag_names = ["tag_0", "tag_1", "tag_2", "tag_3", "tag_4", "tag_5"]
    # Initialize a transform listener
    listener = tf.TransformListener()

    # try navigating to april tag waypoints until all tags are reach
    while len(visited_tags) < len(tag_names):
        # go through each tag name and look up the transform between robot and tag
        for i in len(tag_names):
            try:
                (pose, rot) = listener.lookupTransform('/map',tag_names[i], rospy.Time(0))
                (x, y, z) = pose
                tag_reached = False
                while not tag_reached:
                    tag_reached = move_to_goal(x, y)
                visited_tags[tag_names[i]] = (x, y, z)
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):

