#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/turtlebot/catkin_ws/src/eece5550/mrProject/devel:$CMAKE_PREFIX_PATH"
export PATH="/opt/ros/kinetic/bin:/home/turtlebot/bin:/home/turtlebot/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"
export PWD="/home/turtlebot/catkin_ws/src/eece5550/mrProject/build"
export ROSLISP_PACKAGE_DIRECTORIES="/home/turtlebot/catkin_ws/src/eece5550/mrProject/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/turtlebot/catkin_ws/src/eece5550/mrProject/src:$ROS_PACKAGE_PATH"